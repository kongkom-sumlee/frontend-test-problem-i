import { ApolloClient, HttpLink, InMemoryCache, ApolloQueryResult, gql } from '@apollo/client'
import Countries from '../domains/interfaces/Countries.interface'

class CountryApiService {
    public async getAllCountriesQuery(): Promise<ApolloQueryResult<Countries>> {
        const countryUrl = process.env.REACT_APP_COUNTRY_API_URL
        const client = new ApolloClient({
            link: new HttpLink({
              uri: countryUrl
            }),
            cache: new InMemoryCache(),
        })
        const gqlData = gql`
            {
                countries {
                    name
                    emoji
                    continent {
                        name
                    }
                }
            }
        `

        return await client.query({
            query: gqlData,
        })
    }
    
}

export default new CountryApiService()
