import Countries from '../domains/interfaces/Countries.interface'
import countryApiService from './CountryApiService'
import CountryServiceHelper from './CountryServiceHelper'

class CountryService {
    public async getAllCountries(): Promise<Countries> {
        const rawData = await countryApiService.getAllCountriesQuery()
        const response = CountryServiceHelper.groupCountriesByContinent(rawData.data?.countries)

        return response
    }
    
}

export default new CountryService()
