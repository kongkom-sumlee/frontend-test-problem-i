import Country from '../domains/entities/Country'
import Countries from '../domains/interfaces/Countries.interface'

class CountryServiceHelper {
    public groupCountriesByContinent(countries: Country[]): Countries {
        const countriesByContinent: Countries = {}

        countries.forEach((country: Country) => {
            const continentName = country.continent.name
      
            if (!countriesByContinent[continentName]) {
              countriesByContinent[continentName] = []
            }
      
            countriesByContinent[continentName].push(country)
        })

        return countriesByContinent
    }
}

export default new CountryServiceHelper()
