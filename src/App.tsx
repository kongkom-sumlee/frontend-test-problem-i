import CountryListContainer from './containers/CountryListContainer'
import './App.css'

function App() {
  return (
    <div className="App">
      <h1>World Countries</h1>
      <CountryListContainer />
    </div>
  )
}

export default App
