// src/components/CountryList.tsx

import React from 'react'
import Country from '../../domains/entities/Country'
import './CountryList.css'

const CountryList: React.FC<{ countriesByContinent: Record<string, Country[]> }> = ({ countriesByContinent }) => {
  return (
    <div className="country-list-container">
      {Object.keys(countriesByContinent).map((continentName) => (
        <div key={continentName} className="continent-container">
          <h2 className="continent-header">{continentName}</h2>
          <div className="countries-table">
            {countriesByContinent[continentName].map((country, index) => (
              <div key={index} className="country-item">
                <p className="country-name">{country.name}</p>
                <span className="country-emoji">{country.emoji}</span>
              </div>
            ))}
          </div>
        </div>
      ))}
    </div>
  )
}

export default CountryList
