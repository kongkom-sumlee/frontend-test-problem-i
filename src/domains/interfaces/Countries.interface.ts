import Country from '../entities/Country'

export default interface Countries {
    [key: string]: Country[]
}
