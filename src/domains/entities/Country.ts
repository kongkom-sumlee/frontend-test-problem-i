interface Country {
    name: string
    emoji: string
    continent: {
      name: string
    }
}
  
export default Country
