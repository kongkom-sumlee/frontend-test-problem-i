import countryService from '../../services/CountryService'
import Countries from '../interfaces/Countries.interface'

class GetAllCountriesUseCase {
  public async execute(): Promise<Countries> {
    return await countryService.getAllCountries()
  }
}

export default new GetAllCountriesUseCase()
