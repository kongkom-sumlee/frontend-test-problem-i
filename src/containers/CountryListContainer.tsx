// src/containers/CountryListContainer.tsx

import React, { useEffect, useState } from 'react'
import Country from '../domains/entities/Country'
import CountryList from '../components/CountryList/CountryList'
import getAllCountriesUseCase from '../domains/usecases/GetAllCountriesUseCase'

const CountryListContainer: React.FC = () => {
  const [countriesByContinent, setCountriesByContinent] = useState<Record<string, Country[]>>({})

  useEffect(() => {
    const fetchData = async () => {
      const countriesData = await getAllCountriesUseCase.execute()
      setCountriesByContinent(countriesData)
    }

    fetchData()
  }, [])

  return <CountryList countriesByContinent={countriesByContinent} />
}

export default CountryListContainer
